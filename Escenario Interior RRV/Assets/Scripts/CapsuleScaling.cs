using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    
    [SerializeField]
    private Vector3 axes;
    public float speed;
  
    // Update is called once per frame
    void Update()
    {
      axes = CapsuleMovement.ClampVector3(axes);

      transform.localScale += axes * (speed * Time.deltaTime);  
    }
}

