using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abriendo2 : MonoBehaviour
{
    public Animator puerta2;

    private void OnTriggerEnter(Collider other)
    {
        puerta2.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        puerta2.Play("Close");
    }

}