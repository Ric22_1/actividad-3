using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abriendo4 : MonoBehaviour
{
    public Animator puerta4;

    private void OnTriggerEnter(Collider other)
    {
        puerta4.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        puerta4.Play("Close");
    }

}