using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abriendo5 : MonoBehaviour
{
    public Animator puerta5;

    private void OnTriggerEnter(Collider other)
    {
        puerta5.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        puerta5.Play("Close");
    }

}
