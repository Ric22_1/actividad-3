using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abriendo1 : MonoBehaviour
{
    public Animator puerta1;

    private void OnTriggerEnter(Collider other)
    {
        puerta1.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        puerta1.Play("Close");
    }

}