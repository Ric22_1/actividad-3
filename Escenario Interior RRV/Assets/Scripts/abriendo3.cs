using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abriendo3 : MonoBehaviour
{
    public Animator puerta3;

    private void OnTriggerEnter(Collider other)
    {
        puerta3.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        puerta3.Play("Close");
    }

}